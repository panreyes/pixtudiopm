#AutoIt3Wrapper_Icon=icono_divpm.ico
#AutoIt3Wrapper_Res_Icon_Add=icono_proyecto.ico

#NoTrayIcon

Opt('MustDeclareVars', 1)

#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
#include <EditConstants.au3>
#include <ComboConstants.au3>
#include <MsgBoxConstants.au3>
#include <File.au3>
#include <Crypt.au3>
#include <Date.au3>

Global $version="0.91"
Global $version_date="11.18-18.11.2018"
Global $language="en"

Global $proyecto
Global $proyecto_ini
Global $dcb
Global $fenix_dcb
Global $prg
Global $path_proyecto
Global $compilador_preferido
Global $config_file="etc\config.ini"

Global $txt_ventana
Global $last_gui
Global $msg
Global $button_id

Global $y_form
Global $x_form

Global $path_compilador
Global $path_interprete

Global $path_pxtb="bin\pixtudio-win32\pxtb.exe"
Global $path_pxtp="bin\pixtudio-win32\pxtp.exe"
Global $path_pxtb64="bin\pixtudio-win64\pxtb.exe"
Global $path_pxtp64="bin\pixtudio-win64\pxtp.exe"
Global $path_bgdc="bin\bennu-win\bgdc.exe"
Global $path_bgdi="bin\bennu-win\bgdi.exe"
Global $path_bgdc_classic="bin\bennu-win-classic\bgdc.exe"
Global $path_bgdi_classic="bin\bennu-win-classic\bgdi.exe"
Global $path_bgdc2="bin\bennu2-win64\bgdc.exe"
Global $path_bgdi2="bin\bennu2-win64\bgdi.exe"
Global $path_fxc="bin\fenix-win32\fxc.exe"
Global $path_fxi="bin\fenix-win32\fxi.exe"
Global $path_fenix_stdout="bin\fenix-win32\stdout.txt"
Global $path_fenix_stderr="bin\fenix-win32\stderr.txt"
Global $path_gzip="utils\gzip.exe"
Global $path_7zip="utils\7zip\7z.exe"

#comments-start
;Desactivado invento para actualizar PixTudioPM por problem�tico
if(StringInStr(@ScriptName,"update")) Then
	actualiza()
	Exit
Else
	FileDelete("update.exe")
EndIf
#comments-end

if(StringRight(@MUILang,2)="0a" or StringRight(@OSLang,2)="0a") Then
;	$language="es"
EndIf

FileChangeDir(@ScriptDir)

;Comprobamos si el usuario quiere importar un proyecto:
if($CmdLine[0]>0) Then
	importar_proyecto()
	DirRemove("temp",1)
EndIf

asociar_divpmproject()

Global $last_proyecto = IniRead($config_file,"General","UltimoProyecto","")

if(FileExists("projects\"&$last_proyecto)) Then
	$proyecto=$last_proyecto
EndIf

;Comprobamos actualizaciones:
;Desactivado invento para actualizar PixTudioPM por los posibles problemas de seguridad que supone
#comments-start
if(IniRead($config_file,"General","DesactivarActualizaciones","0")<>1) Then
	Local $version_update=BinaryToString(InetRead("http://divpm.divhub.org/update.php"))
	if($version_update>IniRead($config_file,"General","Version","0")) Then
		if(MsgBox(4,"Actualizaci�n disponible","Hay una actualizaci�n disponible."&@crlf&"�Deseas descargarla y aplicarla?")=6) Then
			FileCopy(@ScriptDir&"\divpm.exe",@ScriptDir&"\update.exe",1)
			Run("update.exe")
			Exit
		Else
			if(MsgBox(4,"Desactivar actualizaciones","�Deseas desactivar las actualizaciones?")=6) Then
				IniWrite($config_file,"General","DesactivarActualizaciones","1")
			EndIf
		EndIf
	EndIf
EndIf
#comments-end


;Bucle principal:
While 1
	if($proyecto="") Then
		elige_proyecto()
	else
		carga_proyecto()
		menu_proyecto()
	EndIf
	Sleep(20)
WEnd

Func carga_proyecto()
	$path_proyecto="projects\"&$proyecto&"\"
	$dcb=$path_proyecto&$proyecto&".dcb"
	$fenix_dcb=$path_proyecto&"src\"&$proyecto&".dcb"
	$prg=$path_proyecto&"src\"&$proyecto&".prg"
	$proyecto_ini=$path_proyecto&"proyecto.ini"
EndFunc

Func error($texto)
	MsgBox(48,t("Error"),$texto)
EndFunc

Func configura_compilador_preferido()
	$compilador_preferido=IniRead($proyecto_ini,"Proyecto","CompiladorPreferido","PixTudio")
	Switch($compilador_preferido)
		Case "PixTudio64"
			$path_compilador=$path_pxtb64
			$path_interprete=$path_pxtp64
		Case "PixTudio"
			$path_compilador=$path_pxtb
			$path_interprete=$path_pxtp
		Case "BennuGD"
			$path_compilador=$path_bgdc
			$path_interprete=$path_bgdi
		Case "BennuGD2"
			$path_compilador=$path_bgdc2
			$path_interprete=$path_bgdi2
		Case "Fenix"
			$path_compilador=$path_fxc
			$path_interprete=$path_fxi
	EndSwitch
EndFunc

Func compila_y_ejecuta()
	if(FileExists($path_proyecto&"src\bgdc.import") and Not FileExists($path_proyecto&"src\pxtb.import")) Then
		FileCopy($path_proyecto&"src\bgdc.import",$path_proyecto&"src\pxtb.import",1)
	EndIf
	if(FileExists($path_proyecto&"src\pxtb.import") and Not FileExists($path_proyecto&"src\bgdc.import")) Then
		FileCopy($path_proyecto&"src\pxtb.import",$path_proyecto&"src\bgdc.import",1)
	EndIf
	FileDelete($dcb)
	FileDelete($path_fenix_stderr)
	FileDelete($path_fenix_stdout)
	if(FileExists($dcb)) Then
		error(t("No ha sido posible compilar el proyecto:")&@crlf&"el fichero DCB est� bloqueado.")
		return
	EndIf
	if($compilador_preferido<>"Fenix") Then ;BennuGD y PixTudio compilan de esta forma
		;Compilamos
		if($compilador_preferido="PixTudio64") Then ;BennuGD y PixTudio compilan de esta forma
			RunWait("cmd /c ..\..\..\"&$path_compilador&" -D __PIXTUDIO__=1 -D __NOTFENIX__=1 "&IniRead($proyecto_ini,"Proyecto","MacrosCompilacion","")&" "&$proyecto&".prg -o ..\"&$proyecto&".dcb 2> %temp%\out.txt",$path_proyecto&"src",@SW_HIDE)
		Elseif($compilador_preferido="BennuGD") Then ;BennuGD y PixTudio compilan de esta forma
			RunWait("cmd /c ..\..\..\"&$path_compilador&" -D __BENNUGD__=1 -D __NOTFENIX__=1 "&IniRead($proyecto_ini,"Proyecto","MacrosCompilacion","")&" "&$proyecto&".prg -o ..\"&$proyecto&".dcb > %temp%\out.txt",$path_proyecto&"src",@SW_HIDE)
		Elseif($compilador_preferido="BennuGD2") Then ;BennuGD2 compila de esta forma
			RunWait("cmd /c ..\..\..\"&$path_compilador&" -D __BENNUGD2__=1 -D __NOTFENIX__=1 "&IniRead($proyecto_ini,"Proyecto","MacrosCompilacion","")&" "&$proyecto&".prg -o ..\"&$proyecto&".dcb > %temp%\out.txt",$path_proyecto&"src",@SW_HIDE)
		Else
			RunWait("cmd /c ..\..\..\"&$path_compilador&" -D __"&StringUpper($compilador_preferido)&"__=1 -D __NOTFENIX__=1 "&IniRead($proyecto_ini,"Proyecto","MacrosCompilacion","")&" "&$proyecto&".prg -o ..\"&$proyecto&".dcb 2> %temp%\out.txt",$path_proyecto&"src",@SW_HIDE)
		EndIf
		if(Not FileExists($dcb)) Then
			error(t("No ha sido posible compilar el proyecto:")&@crlf&@crlf&FileRead(@TempDir&"\out.txt"))
			return
		EndIf

		;Ejecuci�n
		Local $retorno = RunWait("cmd /c ..\..\"&$path_interprete&" "&$proyecto&".dcb 2> %temp%\out.txt",$path_proyecto,@SW_SHOW)
		if($retorno>0) Then
			MsgBox(48,t("Fin de ejecuci�n con errores"),t("Se produjo un error:")&@CRLF&@CRLF&FileRead(@TempDir&"\out.txt"))
		EndIf
	Else ;F�nix sin macros y leyendo los errores de un fichero:
		;Compilamos
		RunWait("cmd /c ..\..\..\"&$path_compilador&" "&$proyecto&".prg",$path_proyecto&"src",@SW_HIDE)
		FileMove($fenix_dcb,$dcb,1)
		if(Not FileExists($dcb)) Then
			error(t("No ha sido posible compilar el proyecto:"&@crlf&@crlf&FileRead($path_fenix_stdout)))
			return
		EndIf

		;Ejecutamos
		Local $retorno = RunWait("cmd /c ..\..\"&$path_interprete&" "&$proyecto&".dcb",$path_proyecto,@SW_SHOW)
		if($retorno>0) Then
			MsgBox(48,t("Fin de ejecuci�n con errores"),t("Se produjo un error:")&@CRLF&@CRLF&FileRead($path_fenix_stderr))
		EndIf
	EndIf
EndFunc

Func compilar_fpgs($force_bpp = 32)
	if($compilador_preferido="Fenix" or $force_bpp=16) Then
		RunWait("cmd /c ..\..\..\"&$path_bgdi_classic&" ..\..\..\utils\pxlfpg.dcb 16",$path_proyecto&"fpg-sources",@SW_HIDE)
		RunWait("cmd /c ..\..\..\"&$path_bgdi_classic&" ..\..\..\utils\pxlfnt.dcb 16",$path_proyecto&"fnt-sources",@SW_HIDE)
	Else
		RunWait("cmd /c ..\..\..\"&$path_bgdi_classic&" ..\..\..\utils\pxlfpg.dcb 32",$path_proyecto&"fpg-sources",@SW_HIDE)
		RunWait("cmd /c ..\..\..\"&$path_bgdi_classic&" ..\..\..\utils\pxlfnt.dcb 32",$path_proyecto&"fnt-sources",@SW_HIDE)
	EndIf

	;�Esto es para descomprimir los FNT? �Es necesario? :?
	#comments-start
	Local $i=0
	Local $file
	Local $aFileList

    $aFileList = _FileListToArray($path_proyecto&"fnt", "*.fnt")
    If @error <> 0 Then
        Return
    EndIf

	Local $free=0
	For $i = 1 to $aFileList[0]
		$free=0
		While($free=0)
			$file=$path_proyecto&"fnt\"&$aFileList[$i]
			FileMove($file,$file&".gz",1)
			RunWait("cmd /c "&$path_gzip&" -d "&$file&".gz",@ScriptDir,@SW_HIDE)
			if(FileExists($file&".gz")) Then
				FileMove($file&".gz",$file,1)
				$free=1
			EndIf
		Wend

		$file=$path_proyecto&"fnt\"&$aFileList[$i]
		RunWait("cmd /c "&$path_gzip&" "&$file,@ScriptDir,@SW_HIDE)
		if(FileExists($file&".gz")) Then
			FileMove($file&".gz",$file,1)
		EndIf
	Next
	#comments-end
EndFunc

Func elige_proyecto()
   $last_gui=""
   GUIDelete()
   GUICreate(t("DIV Project Manager - Elige proyecto"),250,190)

   GUICtrlCreateLabel(t("Abrir proyecto"),10,10,250,30)
   GUICtrlSetFont(-1,15)

   Local $cb_proyecto,$bt_select,$bt_exit,$bt_firma,$bt_create,$bt_delete
   Local $clona_proyecto=""
   Local $proyecto_list
   Local $dir_names=_FileListToArray("projects","*",2)
   Local $i
   Local $nombre_bonito
   For $i = 1 to UBound($dir_names)-1
	   if($dir_names[$i]<>"plantilla" and Not StringInStr($dir_names[$i],"common-src")) Then
	      $proyecto_list=$proyecto_list&"|"&$dir_names[$i]
		  $nombre_bonito=IniRead("projects\"&$dir_names[$i]&"\proyecto.ini","Proyecto","NombreMostrar","")
		  if($nombre_bonito<>"") Then
			  $proyecto_list=$proyecto_list&" ("&$nombre_bonito&")"
		  EndIf
		EndIf
   Next

   $cb_proyecto=GUICtrlCreateCombo("",30,50,190,20,$CBS_DROPDOWNLIST+$WS_VSCROLL)
   GUICtrlSetData($cb_proyecto, $proyecto_list, $last_proyecto)
   $bt_select=GUICtrlCreateButton(t("Abrir"),40,90,70)
   $bt_create=GUICtrlCreateButton(t("Crear nuevo"),130,90,70)
   $bt_firma=GUICtrlCreateButton(t("Crear firma"),40,120,70)
   $bt_delete=GUICtrlCreateButton(t("Borrar proyecto"),130,120,70)
   $bt_exit=GUICtrlCreateButton(t("Salir"),130,150,70)
   GUISetState()

   While 1
	  $msg=GUIGetMsg()
	  Switch $msg
		 Case $GUI_EVENT_CLOSE
			Exit
		Case $bt_select
			$proyecto=GUICtrlRead($cb_proyecto)
			if(StringInStr($proyecto,"(")) Then
				Local $temp=StringSplit($proyecto," (")
				$proyecto=$temp[1]
			EndIf
			ExitLoop
		Case $bt_firma
			generar_firma()
			ExitLoop
		Case $bt_create
			$clona_proyecto="plantilla"
			$proyecto=InputBox(t("Crear proyecto"),t("Introduce el nombre del nuevo proyecto en min�sculas, sin espacios, s�lo letras de la A a la Z y n�meros."))
			if(FileExists("projects\"&$proyecto)) Then
				MsgBox(16,"Error","Ese proyecto ya existe.")
				;ExitLoop
			EndIf
			if($proyecto<>"") Then
				clonar_proyecto($clona_proyecto,$proyecto)
				ExitLoop
			EndIf
		Case $bt_delete
			if(InputBox(t("Confirmaci�n"),t("Escribe delete para confirmar la eliminaci�n"))="delete") Then
				$proyecto=GUICtrlRead($cb_proyecto)
				if(StringInStr($proyecto,"(")) Then
					Local $temp=StringSplit($proyecto," (")
					$proyecto=$temp[1]
				EndIf
				if($proyecto="") Then
					MsgBox(16,t("Error"),t("No se ha encontrado la carpeta del proyecto"))
					ExitLoop
				EndIf
				GUIDelete()
				DirRemove("projects\"&$proyecto,1)
				if(FileExists("projects\"&$proyecto)) Then
					MsgBox(16,t("Error"),t("No se ha podido eliminar la carpeta del proyecto."))
				Else
					MsgBox(0,t("Proyecto eliminado"),t("Proyecto eliminado con �xito."))
				EndIf
				$proyecto=""
				ExitLoop
			EndIf
		Case $bt_exit
			Exit
	  EndSwitch

	  Sleep(10)
   WEnd
   INIWrite($config_file,"General","UltimoProyecto",$proyecto)
   $last_gui=""
   GUIDelete()
EndFunc

Func clonar_proyecto($original,$clona)
	DirCopy("projects\"&$original,"projects\"&$clona)
	FileMove("projects\"&$clona&"\src\"&$original&".prg","projects\"&$clona&"\src\"&$clona&".prg")
EndFunc

Func menu_proyecto()
	;Configuraci�n previa
	configura_compilador_preferido()

   $last_gui=""
   GUIDelete()
   GUICreate(t("DIV Project Manager - Men� de proyecto"),250,300)

   GUICtrlCreateLabel(IniRead($proyecto_ini,"Proyecto","NombreMostrar",$proyecto),10,10,250,30)
   GUICtrlSetFont(-1,15)

   $button_id=0
   Local $bt_compile=GUICtrlCreateButton(t("Compilar y ejecutar"),button_x(),button_y(),110)
   Local $bt_generatefpgs=GUICtrlCreateButton(t("Generar FPGs y FNTs"),button_x(),button_y(),110)
   Local $bt_openfolder=GUICtrlCreateButton(t("Abrir carpeta"),button_x(),button_y(),110)
   Local $bt_opencode=GUICtrlCreateButton(t("Abrir c�digo"),button_x(),button_y(),110)
   Local $bt_generalsettings=GUICtrlCreateButton(t("Ajustes generales"),button_x(),button_y(),110)
   Local $bt_export=GUICtrlCreateButton(t("Exportar"),button_x(),button_y(),110)
   if(FileExists($path_proyecto&".svn") and FileExists("c:\Program Files\TortoiseSVN\bin\TortoiseProc.exe")) Then
	   Local $bt_svncommit=GUICtrlCreateButton(t("SVN Commit"),button_x(),button_y(),110)
	   Local $bt_svnupdate=GUICtrlCreateButton(t("SVN Update"),button_x(),button_y(),110)
   Else
	   Local $bt_svncommit=0
	   Local $bt_svnupdate=0
   EndIf

   ;10/10/19:Desactivado, no es la mejor forma.
   ;Local $bt_convertircodigo=GUICtrlCreateButton(t("Convertir c�digo"),button_x(),button_y(),110)
   Local $bt_convertircodigo=-9999
   Local $bt_firma=GUICtrlCreateButton(t("Crear firma APK"),button_x(),button_y(),110)
   Local $bt_convertirpngsa32bpp=GUICtrlCreateButton(t("Convertir PNGs a 32bpp"),button_x(),button_y(),110)
   Local $bt_close=GUICtrlCreateButton(t("Cerrar proyecto"),button_x(),button_y(),110)
   Local $bt_exit=GUICtrlCreateButton(t("Salir"),button_x(),button_y(),110)
   GUISetState()

   While 1
	  $msg=GUIGetMsg()
	  if($msg<>0) Then
		  Switch $msg
			Case $GUI_EVENT_CLOSE
				Exit
			Case $bt_firma
				Local $temp=generar_firma()
				if($temp<>"") Then
					IniWrite($proyecto_ini,"Android","FicheroAlmacen",$temp)
				EndIf
				ExitLoop
			Case $bt_convertircodigo
				busca_y_convierte()
				ExitLoop
			Case $bt_openfolder
				abrir_carpeta()
				ExitLoop
			Case $bt_opencode
				abrir_codigo()
				ExitLoop
			Case $bt_export
				menu_exportar()
				ExitLoop
			Case $bt_generalsettings
				info_form()
				ExitLoop
			Case $bt_generatefpgs
				estado(t("Compilando FPGs..."))
				compilar_fpgs()
				ExitLoop
			Case $bt_compile
				GUISetState()
				estado(t("Compilando..."))
				compila_y_ejecuta()
				ExitLoop
			Case $bt_svncommit
				svncommit()
				ExitLoop
			Case $bt_svnupdate
				svnupdate()
				ExitLoop
			Case $bt_convertirpngsa32bpp
				;convertir_graficos_a_32bpp(@ScriptDir&"\"&$path_proyecto&"fpg-sources")
				convertir_graficos_a_32bpp(FileSelectFolder(t("Elige la carpeta a convertir"),@ScriptDir&"\"&$path_proyecto&"fpg-sources"))
				ExitLoop
			Case $bt_close
				$proyecto=""
				ExitLoop
			Case $bt_exit
				Exit
		  EndSwitch
	  EndIf

	  Sleep(10)
   WEnd
EndFunc

Func svncommit()
	GUIDelete()
	RunWait("c:\Program Files\TortoiseSVN\bin\TortoiseProc.exe /command:commit /path:"&$path_proyecto)
EndFunc

Func svnupdate()
	GUIDelete()
	RunWait("c:\Program Files\TortoiseSVN\bin\TortoiseProc.exe /command:update /path:"&$path_proyecto)
EndFunc

Func abrir_carpeta()
	ShellExecute($path_proyecto)
EndFunc

Func abrir_codigo()
	ShellExecute($prg)
EndFunc

Func button_x()
	$button_id=$button_id+1
	Return 10+120-(120*Mod($button_id,2))
EndFunc

Func button_y()
	Return 30+(30*Round($button_id/2))
EndFunc

;Cambia el texto de estado de la miniventana
Func estado($mensaje)
	;Si no hemos creado la ventana, la creamos.
	if($last_gui<>"estado") Then
		GUIDelete()
		GUICreate(t("DIV Project Manager"),250,50)
		GUICtrlDelete($txt_ventana)
		$txt_ventana=GUICtrlCreateLabel($mensaje, 15, 13)
		$last_gui="estado"
	Else
		GUICtrlSetData($txt_ventana,$mensaje)
		$txt_ventana=GUICtrlCreateLabel($mensaje, 15, 13)
	EndIf

	;Actualizamos la ventana
	GuiSetState()
EndFunc

Func menu_exportar()
	$last_gui=""
	GUIDelete()
	GUICreate(t("DIV Project Manager - Exportar proyecto"),340, 167)

	GUICtrlCreateLabel(t("Exportar proyecto: ")&$proyecto,10,10,340,30)
	GUICtrlSetFont(-1,15)

	GUICtrlSetFont(-1, 16, 400, 0, "MS Sans Serif")
	GUICtrlCreateLabel(t("Tipo de compilaci�n: "), 32, 43, 100, 17)
	Local $combo_plataforma = GUICtrlCreateCombo("Windows (ZIP)", 136, 40, 113, 25, $CBS_DROPDOWNLIST)
	Switch($compilador_preferido)
		Case "PixTudio"
			GUICtrlSetData(-1, t("Paquete .divpmproject|Linux|Android|OUYA|Raspberry PI (Raspbian)|Windows (Instalador)|Windows (EXE portable)|EmuELEC port (arm,32bit)|Nintendo Switch (ZIP)|Independiente (ZIP)"))
		Case "BennuGD"
			GUICtrlSetData(-1, t("Paquete .divpmproject|Android|Windows (Instalador)|Windows (EXE portable)|EmuELEC port (arm,32bit)|Nintendo Switch (ZIP)|Independiente (ZIP)"))
		Case "BennuGD2"
			GUICtrlSetData(-1, t("Paquete .divpmproject|Windows (Instalador)|Windows (EXE portable)"))
		Case "Fenix"
			GUICtrlSetData(-1, t("Paquete .divpmproject|Windows (Instalador)|Windows (EXE portable)"))
	EndSwitch
	Local $cb_tactil = GUICtrlCreateCheckbox(t("T�ctil"), 136, 64, 105, 25)
	GUICtrlCreateLabel(t("Macros de compilaci�n:"), 16, 93, 116, 17)
	Local $in_macros = GUICtrlCreateInput("", 136, 90, 193, 21)
	Local $bt_exportar = GUICtrlCreateButton(t("Exportar"), 40, 120, 105, 33)
	Local $bt_cancelar = GUICtrlCreateButton(t("Cancelar"), 187, 120, 105, 33)
	GUISetState(@SW_SHOW)

	Local $exportar=0
	Local $regenera_macros=1
	Local $macros
	Local $metodo
	Local $plataforma

	While 1
		$msg = GUIGetMsg()
		Switch $msg
			Case $combo_plataforma
				$regenera_macros=1
			Case $cb_tactil
				$regenera_macros=1
			Case $GUI_EVENT_CLOSE
				ExitLoop
			Case $bt_exportar
				$exportar=1
				ExitLoop
			Case $bt_cancelar
				ExitLoop
		EndSwitch

		if($regenera_macros=1) Then
			$regenera_macros=0
			$macros=""
			If(GUICtrlRead($combo_plataforma)="Android") Then
				GUICtrlSetState($cb_tactil,$GUI_CHECKED)
			Else
				GUICtrlSetState($cb_tactil,$GUI_UNCHECKED)
			EndIf
			$plataforma=StringLower(GUICtrlRead($combo_plataforma))
			if(StringInStr($plataforma,"windows")) Then
				$plataforma="windows"
			EndIf
			if(StringInStr($plataforma,"raspbian")) Then
				$plataforma="raspbian"
			EndIf
			if(StringInStr($plataforma,"emuelec")) Then
				$plataforma="emuelec"
				if($compilador_preferido="BennuGD") Then
					$macros=$macros&"-D __BPP16__=1 "
				EndIf
			EndIf
			if(StringInStr($plataforma,"Nintendo Switch")) Then
				$plataforma="nswitch"
				$macros=$macros&"-D __NINTENDO_SWITCH__=1 "
			EndIf

			if(StringInStr($plataforma,"Independiente")) Then
				$plataforma="independent"
			EndIf

			$macros=$macros&"-D PLATAFORMA="&$plataforma&" "
			if(BitAND(GUICtrlRead($cb_tactil), $GUI_CHECKED) = $GUI_CHECKED) Then
				$macros=$macros&"-D TACTIL=1 "
			EndIf
			$macros=$macros&IniRead($proyecto_ini,"Proyecto","MacrosCompilacion","")
			GUICtrlSetData($in_macros,$macros)
		EndIf
	WEnd

	if($exportar<>1) Then
		Return
	EndIf

	DirCreate($path_proyecto&"out\")

	$macros=GUICtrlRead($in_macros)
	$plataforma=StringLower(GUICtrlRead($combo_plataforma))
	if(StringInStr($plataforma,"Windows")) Then
		if(StringInStr($plataforma,"ZIP")) Then
			$metodo="ZIP"
		EndIf
		if(StringInStr($plataforma,"Instalador") or StringInStr($plataforma,"Installer")) Then
			$metodo="NSIS"
		EndIf
		if(StringInStr($plataforma,"portable")) Then
			$metodo="7zSFX"
		EndIf
		$plataforma="Windows"
	EndIf
	Local $ouya=0

	if(StringInStr($plataforma,"divpmproject")) Then
		$plataforma="divpmproject"
	EndIf

	if($plataforma="ouya") Then
		$ouya=1
		$plataforma="android"
	EndIf

	if(StringInStr($plataforma,"Raspbian")) Then
		$plataforma="Raspbian"
	EndIf

	if(StringInStr($plataforma,"emuelec")) Then
		$plataforma="emuelec"
	EndIf

	if(StringInStr($plataforma,"Nintendo Switch")) Then
		$plataforma="nswitch"
		$metodo="ZIP"
	EndIf

	if(StringInStr($plataforma,"Independiente")) Then
		$plataforma="independent"
	EndIf

	;0. Borrar y crear carpeta Export
	if(ProcessExists("adb.exe")) Then
		estado(t("Cerrando ADB..."))
		ProcessClose("adb.exe")
		if(ProcessExists("adb.exe")) Then
			Sleep(5000)
		EndIf
	EndIf

	estado(t("Borrando anterior carpeta de exportaci�n..."))
	Local $export_path=$path_proyecto&"export\"
	DirRemove($export_path,1)
	if(FileExists($export_path)) Then
		error(t("No ha sido posible borrar la anterior carpeta de exportaci�n."))
		return
	EndIf

	if($plataforma="divpmproject") Then
		;1. Eliminamos recursos generados!
		estado(t("Eliminando recursos regenerables..."))

		;DCB:
		FileDelete($path_proyecto&$proyecto&".dcb")

		;FPGs regenerables desde fpg-sources:
		Local $i=0
		Local $file
		Local $aFileList

		$aFileList = _FileListToArray($path_proyecto&"fpg-sources", "*.",2)
		If(@error=0) Then
			For $i = 1 to $aFileList[0]
				FileDelete($path_proyecto&"fpg\"&$aFileList[$i]&".fpg")
			Next
		EndIf

		;No eliminamos la carpeta OUT!

		;3. Creamos .divpmproject (zip) con lo que queda
		estado(t("Generando archivo .divpmproject..."))
		Local $nombre_zip=$proyecto&"-"&IniRead($proyecto_ini,"Proyecto","Version","1.0")&".divpmproject"
		RunWait("cmd /c ..\"&$path_7zip&" a -t7z "&$nombre_zip&" "&$proyecto&" -xr!out\ -xr!.svn","projects",@SW_SHOW)

		FileMove("projects\"&$nombre_zip,$path_proyecto&"out\",1)
		ShellExecute($path_proyecto&"out\")
		GUIDelete()
		MsgBox(0,t("Mensaje"),t("�Exportaci�n finalizada!"))
		Return
	EndIf ;Fin .divpmproject

	estado(t("Creando carpetas..."))
	DirCreate($export_path)

	;1. Compilar proyecto con flags. Comprobar si se ha generado el DCB correctamente
	estado(t("Compilando c�digo..."))
	FileDelete($dcb)
	if(FileExists($dcb)) Then
		error(t("No ha sido posible compilar el proyecto:"&@crlf&"el fichero DCB est� bloqueado."))
		return
	EndIf
	Switch($compilador_preferido)
		Case "PixTudio64"
			RunWait("cmd /c ..\..\..\"&$path_pxtb64&" -D __PIXTUDIO__=1 -D __NOTFENIX__=1 "&$macros&" "&$proyecto&".prg -o ..\"&$proyecto&".dcb 2> %temp%\out.txt",$path_proyecto&"src",@SW_HIDE)
		Case "PixTudio"
			RunWait("cmd /c ..\..\..\"&$path_pxtb&" -D __"&StringUpper($compilador_preferido)&"__=1 -D __NOTFENIX__=1 "&$macros&" "&$proyecto&".prg -o ..\"&$proyecto&".dcb 2> %temp%\out.txt",$path_proyecto&"src",@SW_HIDE)
		Case "BennuGD"
			RunWait("cmd /c ..\..\..\"&$path_bgdc&" -D __"&StringUpper($compilador_preferido)&"__=1 -D __NOTFENIX__=1 "&$macros&" "&$proyecto&".prg -o ..\"&$proyecto&".dcb 2> %temp%\out.txt",$path_proyecto&"src",@SW_HIDE)
		Case "BennuGD2"
			RunWait("cmd /c ..\..\..\"&$path_bgdc2&" -D __"&StringUpper($compilador_preferido)&"__=1 -D __NOTFENIX__=1 "&$macros&" "&$proyecto&".prg -o ..\"&$proyecto&".dcb 2> %temp%\out.txt",$path_proyecto&"src",@SW_HIDE)
		Case "Fenix"
			RunWait("cmd /c ..\..\..\"&$path_fxc&" "&$proyecto&".prg",$path_proyecto&"src 2> %temp%\out.txt",@SW_HIDE)
			FileMove($fenix_dcb,$dcb,1)
	EndSwitch

	if(Not FileExists($dcb)) Then
		error(t("No ha sido posible compilar el proyecto:")&@crlf&@crlf&FileRead(@TempDir&"\out.txt"))
		return
	EndIf

	;2. Compilar FPGs
	estado(t("Compilando FPGs..."))
	if($plataforma="emuelec" and $compilador_preferido="BennuGD") Then
		compilar_fpgs(16) ;Forzamos compilaci�n a 16bpp
	else
		compilar_fpgs()
	EndIf

	;3. Copiar recursos a �export� o �export\assets�
	estado(t("Copiando recursos del proyecto..."))

	if($plataforma="android") Then
		Switch($compilador_preferido)
			Case "PixTudio"
				DirCopy("bin\pixtudio-android",$export_path,1)
			Case "BennuGD"
				DirCopy("bin\bennu-android",$export_path,1)
		EndSwitch
		$export_path=$path_proyecto&"export\assets\"
		DirCreate($export_path)
	EndIf

	if($plataforma="emuelec") Then
		Switch($compilador_preferido)
			Case "BennuGD"
				DirCopy("bin\bennu-emuelec-arm",$export_path,1)
			Case "PixTudio"
				DirCopy("bin\pixtudio-emuelec-arm",$export_path,1)
				FileMove($path_proyecto&"export\ports\pixtudio\pxtp",$path_proyecto&"export\ports\pixtudio\"&$proyecto)
		EndSwitch

		Local $base_filename=StringLower($compilador_preferido)

		$export_path=$path_proyecto&"export\ports\"&$proyecto&"\"
		DirMove($path_proyecto&"export\ports\"&$base_filename,$export_path,1)
		DirCreate($export_path)

		;Preparamos el archivo .sh
		Local $sh_file=FileRead($path_proyecto&"export\ports\"&$base_filename&".sh")
		FileDelete($path_proyecto&"export\ports\"&$base_filename&".sh")
		$sh_file=StringReplace($sh_file,"NOMBRE_JUEGO",$proyecto)
		FileWrite($path_proyecto&"export\ports\"&$proyecto&".sh",$sh_file) ;351ELEC
		DirCreate($path_proyecto&"export\ports_scripts")
		FileWrite($path_proyecto&"export\ports_scripts\"&$proyecto&".sh",$sh_file) ;EmuELEC

		;Colocamos DCB
		FileCopy($dcb,$export_path,1)
	EndIf

	if($plataforma="nswitch") Then
		FileCopy($dcb,$export_path&"main.dcb",1)
	EndIf

	if($plataforma="independent") Then
		FileCopy($dcb,$export_path,1)
	EndIf

	FileCopy($path_proyecto&"*.png",$export_path,1)
	FileCopy($path_proyecto&"*.fpg",$export_path,1)
	FileCopy($path_proyecto&"*.fnt",$export_path,1)
	FileCopy($path_proyecto&"*.wav",$export_path,1)
	FileCopy($path_proyecto&"*.ogg",$export_path,1)
	FileCopy($path_proyecto&"*.ogv",$export_path,1)
	FileCopy($path_proyecto&"*.ttf",$export_path,1)
	FileCopy($path_proyecto&"*.webp",$export_path,1)
	FileCopy($path_proyecto&"*.jpg",$export_path,1)
	DirCopy($path_proyecto&"anims",$export_path&"anims",1)
	DirCopy($path_proyecto&"fnt",$export_path&"fnt",1)
	DirCopy($path_proyecto&"fpg",$export_path&"fpg",1)
	DirCopy($path_proyecto&"ogg",$export_path&"ogg",1)
	DirCopy($path_proyecto&"ogv",$export_path&"ogv",1)
	DirCopy($path_proyecto&"wav",$export_path&"wav",1)
	DirCopy($path_proyecto&"png",$export_path&"png",1)
	DirCopy($path_proyecto&"ttf",$export_path&"ttf",1)
	DirCopy($path_proyecto&"webp",$export_path&"webp",1)
	DirCopy($path_proyecto&"jpg",$export_path&"jpg",1)

	Local $carpetas_extra=IniRead($proyecto_ini,"Proyecto","CarpetasExtra","")
	if($carpetas_extra<>"") Then
		if(StringInStr($carpetas_extra,"|")) Then
			Local $carpetas_extras=StringSplit($carpetas_extra,"|")
			Local $i
			For $i=1 to $carpetas_extras[0]
				if($carpetas_extras[$i]<>"") Then
					DirCopy($path_proyecto&$carpetas_extras[$i],$export_path&$carpetas_extras[$i],1)
				EndIf
			Next
		Else
			DirCopy($path_proyecto&$carpetas_extra,$export_path&$carpetas_extra,1)
		EndIf
	EndIf

	;3.1 Android: Copiar recursos Android, generar archivos xmls y carpetas.
	if($plataforma="android") Then
		estado(t("Descomprimiendo FPGs y FNTs..."))
		if($compilador_preferido="PixTudio") Then

		Else

		endif
		descomprimir_recursos()

		estado(t("Generando XMLs..."))

		;AndroidManifest.xml
		Local $version_code=IniRead($proyecto_ini,"Android","VersionCode","0")
		$version_code=$version_code+1
		IniWrite($proyecto_ini,"Android","VersionCode",$version_code)


		if($ouya) Then
			Local $orientacion=StringLower(IniRead($proyecto_ini,"Android","OrientacionOUYA","Horizontal"))
		Else
			Local $orientacion=StringLower(IniRead($proyecto_ini,"Android","Orientacion","Horizontal"))
		EndIf
		if($orientacion="horizontal") Then
			$orientacion="landscape"
		Else
			$orientacion="portrait"
		EndIf

		Local $xml=$path_proyecto&"export\AndroidManifest.xml"
		FileDelete($xml)
		FileWriteLine($xml,'<?xml version="1.0" encoding="utf-8"?>')
		FileWriteLine($xml,'<manifest xmlns:android="http://schemas.android.com/apk/res/android"')
		FileWriteLine($xml,'package="'&IniRead($proyecto_ini,"Android","NombrePaquete","org.example."&$proyecto)&'"')
		FileWriteLine($xml,'android:versionCode="'&$version_code&'"')
		FileWriteLine($xml,'android:versionName="'&IniRead($proyecto_ini,"Proyecto","Version","1.0")&'"')
		FileWriteLine($xml,'android:installLocation="auto">')
		FileWriteLine($xml,'<application android:label="@string/app_name" android:icon="@drawable/icon" android:theme="@android:style/Theme.NoTitleBar.Fullscreen">')
		FileWriteLine($xml,'<activity android:name="'&$proyecto&'" android:label="@string/app_name" android:screenOrientation="'&$orientacion&'">')
		FileWriteLine($xml,'<intent-filter>')
		FileWriteLine($xml,'<action android:name="android.intent.action.MAIN" />')
		FileWriteLine($xml,'<category android:name="android.intent.category.LAUNCHER" />')
		FileWriteLine($xml,'<category android:name="tv.ouya.intent.category.GAME"/>')
		FileWriteLine($xml,'</intent-filter>')
		FileWriteLine($xml,'</activity>')
		FileWriteLine($xml,'</application>')
		FileWriteLine($xml,'<uses-sdk android:minSdkVersion="12" android:targetSdkVersion="19"/>')
		FileWriteLine($xml,'<uses-feature android:glEsVersion="0x00020000" />')

		Local $permisos=IniRead($proyecto_ini,"Android","Permisos","")
		if($permisos<>"") Then
			if(StringInStr($permisos,"|")) Then
				Local $permisoss=StringSplit($permisos,"|")
				Local $i
				For $i=1 to $permisoss[0]
					if($permisoss[$i]<>"") Then
						FileWriteLine($xml,'<uses-permission android:name="'&$permisoss[$i]&'" />')
					EndIf
				Next
			Else
				FileWriteLine($xml,'<uses-permission android:name="'&$permisos&'" />')
			EndIf
		EndIf

		FileWriteLine($xml,'</manifest>')

		;build.xml
		Local $xml=$path_proyecto&"export\build.xml"
		FileDelete($xml)
		FileWriteLine($xml,'<?xml version="1.0" encoding="utf-8"?>')
		FileWriteLine($xml,'<project name="'&$proyecto&'" default="help">')
		FileWriteLine($xml,'<loadproperties srcFile="local.properties"/>')
		FileWriteLine($xml,'<property file="build.properties"/>')
		FileWriteLine($xml,'<property file="default.properties"/>')
		FileWriteLine($xml,'<loadproperties srcFile="default.properties"/>')
		FileWriteLine($xml,'<import file="${sdk.dir}/tools/ant/build.xml" />')
		FileWriteLine($xml,'<path id="android.antlibs">')
		FileWriteLine($xml,'<pathelement path="${sdk.dir}/tools/lib/anttasks.jar" />')
		FileWriteLine($xml,'<pathelement path="${sdk.dir}/tools/lib/sdklib.jar" />')
		FileWriteLine($xml,'<pathelement path="${sdk.dir}/tools/lib/androidprefs.jar" />')
		FileWriteLine($xml,'<pathelement path="${sdk.dir}/tools/lib/apkbuilder.jar" />')
		FileWriteLine($xml,'<pathelement path="${sdk.dir}/tools/lib/jarutils.jar" />')
		FileWriteLine($xml,'</path>')
		FileWriteLine($xml,'<!--<taskdef name="setup"-->')
		FileWriteLine($xml,'<!--        classname="com.android.ant.SetupTask"-->')
		FileWriteLine($xml,'<!--        classpathref="android.antlibs" />-->')
		FileWriteLine($xml,'<!--<setup />-->')
		FileWriteLine($xml,'</project>')

		;strings.xml
		Local $xml=$path_proyecto&"export\res\values\strings.xml"
		FileDelete($xml)
		FileWriteLine($xml,'<?xml version="1.0" encoding="utf-8"?>')
		FileWriteLine($xml,'<resources>')
		FileWriteLine($xml,'<string name="app_name">'&IniRead($proyecto_ini,"Proyecto","NombreMostrar",$proyecto)&'</string>')
		FileWriteLine($xml,'</resources>')

		;local.properties
		Local $xml=$path_proyecto&"export\local.properties"
		FileDelete($xml)
		FileWriteLine($xml,'sdk.dir=../../../utils/android-sdk')

		;build.properties
		Local $xml=$path_proyecto&"export\build.properties"
		FileDelete($xml)
		FileWriteLine($xml,'')

		;si hay firma:
		Local $firma=IniRead($proyecto_ini,"Android","FicheroAlmacen","")
		if($firma<>"") Then
			FileWriteLine($xml,'key.store=../../../etc/keys/'&$firma&'.keystore')
			FileWriteLine($xml,'key.alias='&IniRead('etc/keys/'&$firma&'.ini',"Firma","Alias",""))
			FileWriteLine($xml,'key.store.password='&IniRead('etc/keys/'&$firma&'.ini',"Firma","Pass",""))
			FileWriteLine($xml,'key.alias.password='&IniRead('etc/keys/'&$firma&'.ini',"Firma","Pass",""))
		EndIf

		;paquete de android
		Local $src_path=$path_proyecto&"export\src\"&StringReplace(IniRead($proyecto_ini,"Android","NombrePaquete","org.example."&$proyecto),".","\")
		Local $java=$src_path&"\"&$proyecto&".java"
		DirCreate($src_path)
		FileDelete($java)
		FileWriteLine($java,'package '&IniRead($proyecto_ini,"Android","NombrePaquete","org.example."&$proyecto)&';')
		FileWriteLine($java,'')
		FileWriteLine($java,'import org.libsdl.app.SDLActivity;')
		FileWriteLine($java,'import android.os.*;')
		FileWriteLine($java,'import android.content.*;')
		FileWriteLine($java,'import android.net.Uri;')
		FileWriteLine($java,'')
		FileWriteLine($java,'public class '&$proyecto&' extends SDLActivity {')
		FileWriteLine($java,'    public static void openURL(String url) {')
		FileWriteLine($java,'        Intent i = new Intent(Intent.ACTION_VIEW);')
		FileWriteLine($java,'        i.setData(Uri.parse(url));')
		FileWriteLine($java,'        mSingleton.startActivity(i);')
		FileWriteLine($java,'    }')
		FileWriteLine($java,'}')

		estado("Copiando iconos...")
		FileCopy($path_proyecto&"recursos\android\ldpi.png",$path_proyecto&"export\res\drawable-ldpi\icon.png",1)
		FileCopy($path_proyecto&"recursos\android\hdpi.png",$path_proyecto&"export\res\drawable-hdpi\icon.png",1)
		FileCopy($path_proyecto&"recursos\android\mdpi.png",$path_proyecto&"export\res\drawable-mdpi\icon.png",1)
		FileCopy($path_proyecto&"recursos\android\xhdpi.png",$path_proyecto&"export\res\drawable-xhdpi\icon.png",1)
		FileCopy($path_proyecto&"recursos\android\ouya_icon.png",$path_proyecto&"export\res\drawable-xhdpi\ouya_icon.png",1)

		FileCopy($dcb,$export_path&"main.dcb",1)
	EndIf

	;3.2 Windows: Copia int�rprete
	if($plataforma="windows") Then
		estado(t("Copiando ejecutables..."))
		Switch($compilador_preferido)
			Case "PixTudio64"
				FileCopy("bin\pixtudio-win64\*.dll",$export_path,1)
				if(FileExists($path_proyecto&"recursos\win32\1.ico")) Then
					RunWait("utils\reshacker\reshacker.exe -addoverwrite bin\pixtudio-win64\pxtp.exe, "&$export_path&$proyecto&".exe, "&$path_proyecto&"recursos\win32\1.ico, ICONGROUP,IDR_MAINFRAME,0")
				Else
					FileCopy("bin\pixtudio-win64\pxtp.exe",$export_path&$proyecto&".exe",1)
				EndIf
			Case "PixTudio"
				FileCopy("bin\pixtudio-win32\*.dll",$export_path,1)
				if(FileExists($path_proyecto&"recursos\win32\1.ico")) Then
					RunWait("utils\reshacker\reshacker.exe -addoverwrite bin\pixtudio-win32\pxtp.exe, "&$export_path&$proyecto&".exe, "&$path_proyecto&"recursos\win32\1.ico, ICONGROUP,IDR_MAINFRAME,0")
				Else
					FileCopy("bin\pixtudio-win32\pxtp.exe",$export_path&$proyecto&".exe",1)
				EndIf
			Case "BennuGD"
				FileCopy("bin\bennu-win\*.dll",$export_path,1)
				if(FileExists($path_proyecto&"recursos\win32\1.ico")) Then
					RunWait("utils\reshacker\reshacker.exe -addoverwrite bin\bennu-win\bgdi.exe, "&$export_path&$proyecto&".exe, "&$path_proyecto&"recursos\win32\1.ico, ICONGROUP,IDR_MAINFRAME,0")
				Else
					FileCopy("bin\bennu-win\bgdi.exe",$export_path&$proyecto&".exe",1)
				EndIf
			Case "BennuGD2"
				FileCopy("bin\bennu2-win64\*.dll",$export_path,1)
				if(FileExists($path_proyecto&"recursos\win32\1.ico")) Then
					RunWait("utils\reshacker\reshacker.exe -addoverwrite bin\bennu2-win64\bgdi.exe, "&$export_path&$proyecto&".exe, "&$path_proyecto&"recursos\win32\1.ico, ICONGROUP,IDR_MAINFRAME,0")
				Else
					FileCopy("bin\bennu2-win64\bgdi.exe",$export_path&$proyecto&".exe",1)
				EndIf
			Case "Fenix"
				FileCopy("bin\fenix-win32\*.dll",$export_path,1)
				if(FileExists($path_proyecto&"recursos\win32\1.ico")) Then
					RunWait("utils\reshacker\reshacker.exe -addoverwrite bin\fenix-win32\fxi.exe, "&$export_path&$proyecto&".exe, "&$path_proyecto&"recursos\win32\1.ico, ICONGROUP,IDR_MAINFRAME,0")
				Else
					FileCopy("bin\fenix-win32\fxi.exe",$export_path&$proyecto&".exe",1)
				EndIf
		EndSwitch
		FileCopy($dcb,$export_path,1)
	EndIf

	;3.3 Linux: Copia binarios de pixtudio
	if($plataforma="linux") Then
		estado(t("Copiando binarios de PixTudio..."))
		FileCopy("bin\pixtudio-linux32\*.so",$export_path,1)
		FileCopy("bin\pixtudio-linux32\pxtp",$export_path&$proyecto,1)
		FileCopy($dcb,$export_path,1)
	EndIf

	;3.4 Raspberry Linux: Copia binarios de PixTudio
	if($plataforma="raspbian") Then
		estado(t("Copiando binarios de PixTudio..."))
		FileCopy("bin\pixtudio-raspbian\pxtp",$export_path&$proyecto,1)
		FileCopy($dcb,$export_path,1)
	EndIf

	;4.0 Empaquetado

	;4.1 Windows: Crear ZIP
	if($plataforma="windows") Then
		if($metodo="ZIP") Then
			estado(t("Empaquetando proyecto en ZIP..."))
			$export_path=$proyecto&"-"&IniRead($proyecto_ini,"Proyecto","Version","1.0")&"-win32"
			Local $zip_destino=$export_path&".zip"
			Local $zip_destino_largo=$path_proyecto&"out\"&$zip_destino
			Local $export_path_largo=$path_proyecto&"out\"&$export_path

			DirRemove($export_path_largo,1)
			DirMove($path_proyecto&"export",$export_path_largo)
			FileDelete($zip_destino_largo)

			RunWait("cmd /c ..\..\..\"&$path_7zip&" a "&$zip_destino&" "&$export_path,$path_proyecto&"out\",@SW_HIDE)

			if(FileExists($zip_destino_largo)) Then
				DirRemove($export_path_largo,1)
			Else
				error(t("No se ha podido crear el archivo ZIP."))
			EndIf
			ShellExecute($path_proyecto&"out\")
		EndIf
		if($metodo="NSIS") Then
			estado(t("Creando instalador NSIS..."))
			generar_nsis()
			ShellExecute($path_proyecto&"out\")
		EndIf
		if($metodo="7zSFX") Then
			estado(t("Creando ejecutable portable..."))
			generar_config_txt_7zip()
			RunWait("cmd /c ..\..\..\"&$path_7zip&" a -m0=LZMA %temp%\temp.7z",$export_path,@SW_HIDE)
			RunWait("cmd /c copy /y /b ..\..\..\utils\7zip\7zsd.sfx + config.txt + %temp%\temp.7z ..\out\"&$proyecto&"-"&IniRead($proyecto_ini,"Proyecto","Version","1.0")&"-win32.exe",$export_path,@SW_HIDE)
			FileDelete(@TempDir&"\temp.7z")
			if(Not FileExists($path_proyecto&"out\"&$proyecto&"-"&IniRead($proyecto_ini,"Proyecto","Version","1.0")&"-win32.exe")) Then
				error(t("No se ha podido crear el archivo EXE."))
			EndIf
			ShellExecute($path_proyecto&"out\")
		EndIf
	EndIf

	;4.2 Independiente/Linux/Raspbian/EmuELEC: Crear ZIP
	if($plataforma="independent" or $plataforma="linux" or $plataforma="raspbian" or $plataforma="emuelec" or $plataforma="nswitch") Then
		estado(t("Empaquetando proyecto en ZIP..."))
		#comments-start
		Switch($plataforma)
			Case "linux"
				$export_path=$proyecto&"-"&IniRead($proyecto_ini,"Proyecto","Version","1.0")&"-linux32"
			Case "raspbian"
				$export_path=$proyecto&"-"&IniRead($proyecto_ini,"Proyecto","Version","1.0")&"-raspbian"
			Case "emuelec"
				$export_path=$proyecto&"-"&IniRead($proyecto_ini,"Proyecto","Version","1.0")&"-emuelec-arm"
		EndSwitch
		#comments-end
		$export_path=$proyecto&"-"&IniRead($proyecto_ini,"Proyecto","Version","1.0")&"-"&$plataforma
		Local $zip_destino=$export_path&".zip"
		Local $zip_destino_largo=$path_proyecto&"out\"&$zip_destino
		Local $export_path_largo=$path_proyecto&"out\"&$export_path

		DirRemove($export_path_largo,1)
		DirMove($path_proyecto&"export",$export_path_largo)
		FileDelete($zip_destino_largo)

		RunWait("cmd /c ..\..\..\"&$path_7zip&" a "&$zip_destino&" "&$export_path,$path_proyecto&"out\",@SW_HIDE)

		if(FileExists($zip_destino_largo)) Then
			DirRemove($export_path_largo,1)
		Else
			error(t("No se ha podido crear el archivo ZIP."))
		EndIf
		ShellExecute($path_proyecto&"out\")
	EndIf

	;4.3 Android: Crear APK
	if($plataforma="android") Then
		estado(t("Empaquetando APK..."))
		;env variables:
		EnvSet("dirbase",@ScriptDir&"\utils")
		EnvSet("java_home",@ScriptDir&"\utils\jdk")
		EnvSet("path",EnvGet("path")&";"&@ScriptDir&"\utils\jdk;"&@ScriptDir&"\utils\jdk\bin;"&@ScriptDir&"\utils\jdk\jre;"&@ScriptDir&"\utils\jdk\jre\bin;"&@ScriptDir&"\utils;"&@ScriptDir&"\utils\android-sdk\platform-tools;"&@ScriptDir&"\utils\ant\bin;")
		EnvSet("ant_home",@ScriptDir&"\utils\ant")
		EnvSet("java_exe",@ScriptDir&"\utils\jdk\jre\bin\java.exe")
		EnvSet("javacmd",@ScriptDir&"\utils\jdk\jre\bin\java.exe")

		;si hay firma, release. Sino, debug:
		Local $tipo_compilacion
		if($firma<>"") Then
			$tipo_compilacion="release"
		Else
			$tipo_compilacion="debug"
		EndIf
		;RunWait("cmd /c ant "&$tipo_compilacion&" install",$path_proyecto&"export",@SW_SHOW)
		RunWait("cmd /c ant "&$tipo_compilacion,$path_proyecto&"export",@SW_SHOW)
		ProcessClose("adb.exe")
		If(FileExists($path_proyecto&"export\bin\"&$proyecto&"-"&$tipo_compilacion&".apk")) Then
			FileMove($path_proyecto&"export\bin\"&$proyecto&"-"&$tipo_compilacion&".apk",$path_proyecto&"out\"&$proyecto&"-"&IniRead($proyecto_ini,"Proyecto","Version","1.0")&"-android.apk",1)
			ShellExecute($path_proyecto&"out\")
		Else
			RunWait("cmd /c ant "&$tipo_compilacion&" install -l %temp%\out.txt",$path_proyecto&"export",@SW_SHOW)
			Run("cmd /c notepad %temp%\out.txt")
			error(t("No se ha generado el paquete APK correctamente."))
			return
		EndIf
	EndIf

	MsgBox(0,t("Mensaje"),t("�Exportaci�n finalizada!"))

EndFunc

Func descomprimir_recursos()
	Local $i=0
	Local $file
	Local $aFileList

    $aFileList = _FileListToArray($path_proyecto&"fpg", "*.fpg")
    If @error <> 0 Then
        Return
    EndIf

	For $i = 1 to $aFileList[0]
		$file=$path_proyecto&"export\assets\fpg\"&$aFileList[$i]
		FileMove($file,$file&".gz",1)
		RunWait($path_gzip&" -d "&$file&".gz",@ScriptDir,@SW_HIDE)
		if(FileExists($file&".gz")) Then
			FileMove($file&".gz",$file,1)
		EndIf
	Next

    $aFileList = _FileListToArray($path_proyecto&"fnt", "*.fnt")
    If @error <> 0 Then
        Return
    EndIf

	For $i = 1 to $aFileList[0]
		$file=$path_proyecto&"export\assets\fnt\"&$aFileList[$i]
		FileMove($file,$file&".gz",1)
		RunWait($path_gzip&" -d "&$file&".gz",@ScriptDir,@SW_HIDE)
		if(FileExists($file&".gz")) Then
			FileMove($file&".gz",$file,1)
		EndIf
	Next
EndFunc

Func info_form()
	Local $msg,$form_save,$x,$close

	$x_form=0
	$y_form=-40

	$last_gui=""
    GUIDelete()
    GUICreate(t("DIV Project Manager - Ajustes generales"), 640, 600)

    title(t("Informaci�n del proyecto"))
    Local $in_nicename=field(t("Nombre a mostrar"),"Proyecto","NombreMostrar",$proyecto,100)
	Local $in_author=field(t("Autor"),"Proyecto","Autor","",100)
	Local $in_version=field(t("Version"),"Proyecto","Version","1.0",100)
	Local $in_compilador=field_combo(t("Compilador preferido"),"Proyecto","CompiladorPreferido","PixTudio",100,"PixTudio|PixTudio64|BennuGD2|BennuGD|Fenix")
	Local $in_macros=field(t("Macros de compilaci�n"),"Proyecto","MacrosCompilacion","",180)
	Local $in_carpetas=field_ml(t("Carpetas extra a copiar"),"Proyecto","CarpetasExtra","",180)
	GUICtrlSetData($in_carpetas,StringReplace(GUICtrlRead($in_carpetas),"|",@CRLF))

    $x_form=320
	$y_form=-40

	;Android
	title("Android")
	Local $in_nombrepaquete=field(t("Nombre del paquete"),"Android","NombrePaquete","org.pixtudio.plantilla",180)
	Local $in_orientacion=field_combo(t("Orientaci�n"),"Android","Orientacion","Horizontal",100,"Horizontal|Vertical")
	Local $in_permisos=field_ml(t("Permisos"),"Android","Permisos","",180)
	GUICtrlSetData($in_permisos,StringReplace(GUICtrlRead($in_permisos),"|",@CRLF))
	;Local $in_ficheroalmacen=field("Firma APK","Android","FicheroAlmacen","",100)

	;lista de firmas
	Local $i=0
	Local $file
	Local $aFileList
	Local $firmas

    $aFileList = _FileListToArray("etc\keys", "*.keystore")
    If @error = 0 Then
		For $i = 1 to $aFileList[0]
			$aFileList[$i]=StringReplace($aFileList[$i],".keystore","")
			$firmas=$firmas&$aFileList[$i]&"|"
		Next
		$firmas=StringLeft($firmas,StringLen($firmas)-1)
	EndIf

	Local $in_ficheroalmacen=field_combo(t("Firma APK"),"Android","FicheroAlmacen","",100,$firmas)
	Local $in_orientacion_ouya=field_combo(t("Orientaci�n OUYA"),"Android","OrientacionOUYA","Horizontal",100,"Horizontal|Vertical")

    $y_form=$y_form+300

    $form_save = GUICtrlCreateButton(t("Guardar"), 360, 500, 90, 30)
	$close = GUICtrlCreateButton(t("Cancelar"), 500, 500, 90, 30)

    GUISetState()

    $msg = 0
    While $msg <> $GUI_EVENT_CLOSE
        $msg = GUIGetMsg()
		if($msg = $form_save) Then
			GUICtrlSetData($in_carpetas,StringReplace(GUICtrlRead($in_carpetas),@CRLF,"|"))
			GUICtrlSetData($in_permisos,StringReplace(GUICtrlRead($in_permisos),@CRLF,"|"))

			guarda($in_nicename,"Proyecto","NombreMostrar")
			guarda($in_author,"Proyecto","Autor")
			guarda($in_version,"Proyecto","Version")
			guarda($in_compilador,"Proyecto","CompiladorPreferido")
			guarda($in_macros,"Proyecto","MacrosCompilacion")
			guarda($in_carpetas,"Proyecto","CarpetasExtra")

			guarda($in_nombrepaquete,"Android","NombrePaquete")
			guarda($in_orientacion,"Android","Orientacion")
			guarda($in_permisos,"Android","Permisos")
			guarda($in_ficheroalmacen,"Android","FicheroAlmacen")
			guarda($in_orientacion_ouya,"Android","OrientacionOUYA")
		EndIf
        Select
			Case $msg = $form_save
			    ExitLoop
			Case $msg = $close
			    ExitLoop
        EndSelect
	 WEnd
EndFunc

Func y_form()
	$y_form=$y_form+30
	Return $y_form
EndFunc

Func field($label,$section,$key,$default,$size)
   y_form()
   GUICtrlCreateLabel($label,$x_form+10,$y_form+3,120,0)
   Return GUICtrlCreateInput(IniRead($proyecto_ini,$section,$key,$default),$x_form+120, $y_form, $size, 20)
EndFunc

Func field_ml($label,$section,$key,$default,$size)
   y_form()
   GUICtrlCreateLabel($label,$x_form+10,$y_form+3,120,0)
   Local $id=GUICtrlCreateInput(IniRead($proyecto_ini,$section,$key,$default),$x_form+120, $y_form, $size, 60,BitOR($ES_MULTILINE,$ES_WANTRETURN))
   y_form()
   Return $id
EndFunc

Func field_combo($label,$section,$key,$default,$size,$content)
	y_form()
	GUICtrlCreateLabel($label,$x_form+10,$y_form+3,120,0)
	Local $id=GUICtrlCreateCombo("",$x_form+120, $y_form, $size, 20,$CBS_DROPDOWNLIST)
	GUICtrlSetData($id,$content,IniRead($proyecto_ini,$section,$key,$default))
	Return $id
EndFunc

Func title($title)
    $y_form=$y_form+20
    GUICtrlCreateLabel($title,$x_form+25,y_form(),250,30)
    GUICtrlSetFont(-1,15)
 EndFunc

Func guarda($id_control,$section,$key)
   IniWrite($proyecto_ini,$section,$key,GUICtrlRead($id_control))
EndFunc

Func busca_y_convierte()
	estado(t("Convirtiendo c�digo a PixTudio..."))

	Local $i=0, $j=0
	Local $file
	Local $aFileList
	Local $bFileList

    $aFileList = _FileListToArray($path_proyecto&"src", "*.*")
    If @error <> 0 Then
        Return
    EndIf

	$bFileList = _FileListToArray("etc\reemplazos", "*.*")
    If @error <> 0 Then
        Return
    EndIf
	Local $reemplazos

	For $j = 1 to $bFileList[0] ;reemplazos
		$reemplazos="etc\reemplazos\"&$bFileList[$j]
		For $i = 1 to $aFileList[0] ;prgs
			$file=$path_proyecto&"src\"&$aFileList[$i]
			convierte_prg($file,$reemplazos)
		Next
	Next
EndFunc

Func convierte_prg($file,$reemplazos)
	Local $texto = FileRead($file)
	Local $fp
	Local $linea

	$fp=FileOpen($reemplazos)
	While 1
		$linea=FileReadLine($fp)
		if(@error<>0) Then
			ExitLoop
		EndIf
		if(StringInStr($linea," ||| ")) Then
			Local $temp=StringSplit($linea," ||| ",$STR_ENTIRESPLIT)
			$texto=StringReplace($texto,$temp[1],$temp[2])
		EndIf
	WEnd
	FileClose($fp)

	$fp=FileOpen($file,$FO_OVERWRITE)
	FileWrite($fp,$texto)
	FileClose($fp)
EndFunc

Func generar_firma()
	GUIDelete()
	Local $nombre=InputBox(t("Generando firma para APK"),t("Introduce el nombre que le quieras dar a la firma (por ejemplo: pixjuegos):"))
	if($nombre="") Then
		MsgBox(48,t("Generando firma para APK"),t("Proceso cancelado, firma no creada"))
		Return
	EndIf

	if(FileExists("etc\keys\"&$nombre&".keystore")) Then
		MsgBox(48,t("Generando firma para APK"),t("Error: Ya existe una firma con ese nombre."))
		Return
	EndIf

	Local $pass=InputBox(t("Generando firma para APK"),t("Introduce una contrase�a de 6 o m�s caracteres:"))
	if($pass="" or StringLen($pass)<6) Then
		MsgBox(48,t("Generando firma para APK"),t("Proceso cancelado, firma no creada"))
		Return
	EndIf

	MsgBox(0,t("Generando firma para APK"),t("A continuaci�n se ejecutar� la herramienta de creaci�n de firmas digitales y te pedir� ciertos datos. Pulsa Aceptar para continuar."))

	;Generando firma
	RunWait("utils\jdk\bin\keytool.exe -genkey -v -keystore etc\keys\"&$nombre&".keystore -alias "&$nombre&" -keyalg RSA -keysize 2048 -validity 10000 -storepass "&$pass&" -keypass "&$pass)

	if(FileExists("etc\keys\"&$nombre&".keystore")) Then
		IniWrite("etc\keys\"&$nombre&".ini","Firma","Pass",$pass)
		IniWrite("etc\keys\"&$nombre&".ini","Firma","Alias",$nombre)
		MsgBox(64,"Generando firma para APK","Firma generada correctamente.")
		Return $nombre
	EndIf
EndFunc

Func generar_config_txt_7zip()
	Local $config_txt_7zip=$path_proyecto&"export\config.txt"

	FileWriteLine($config_txt_7zip,';!@Install@!UTF-8!')
	FileWriteLine($config_txt_7zip,'Title="'&IniRead($proyecto_ini,"Proyecto","NombreMostrar","proyecto")&'"')
	FileWriteLine($config_txt_7zip,'ExtractTitle="'&IniRead($proyecto_ini,"Proyecto","NombreMostrar","proyecto")&'"')
	FileWriteLine($config_txt_7zip,'InstallPath="%temp%\\'&$proyecto&'"')
	FileWriteLine($config_txt_7zip,'Directory="%temp%\\'&$proyecto&'"')
	FileWriteLine($config_txt_7zip,'RunProgram="hidcon:'&$proyecto&'.exe"')
	FileWriteLine($config_txt_7zip,'RunProgram="hidcon:cmd /c cd .. && rd /s /q %temp%\\'&$proyecto&'"')
	FileWriteLine($config_txt_7zip,'GUIMode="1"')
	FileWriteLine($config_txt_7zip,';!@InstallEnd@!')
EndFunc

Func generar_nsis()
	Local $export_path=$path_proyecto&"export\"
	Local $nsis_file=$export_path&"setup.nsi"

	Local $nombre_mostrar=IniRead($proyecto_ini,"Proyecto","NombreMostrar","proyecto")
	;Local $out_path=$path_proyecto&"export\"&$nombre_mostrar
	Local $autor=IniRead($proyecto_ini,"Proyecto","Autor","autor")
	Local $ver=IniRead($proyecto_ini,"Proyecto","Version","1.0")

	;DirMove($path_proyecto&"export\",$path_proyecto&"export2\")
	;DirCreate($path_proyecto&"export\")
	;DirMove($path_proyecto&"export2\",$out_path)
	FileDelete($nsis_file)

	$nsis_file=FileOpen($nsis_file,$FO_OVERWRITE+$FO_ANSI)
	FileWriteLine($nsis_file,'RequestExecutionLevel admin');
	FileWriteLine($nsis_file,'');
	FileWriteLine($nsis_file,'!define PRODUCT_NAME "'&$nombre_mostrar&'"');
	FileWriteLine($nsis_file,'!define PRODUCT_VERSION "'&$ver&'"');
	FileWriteLine($nsis_file,'!define PRODUCT_PUBLISHER "'&$autor&'"');
	if(IniRead($proyecto_ini,"Proyecto","Web","")<>"") Then
		FileWriteLine($nsis_file,'!define PRODUCT_WEB_SITE "'&IniRead($proyecto_ini,"Proyecto","Web","")&'"');
	EndIf
	FileWriteLine($nsis_file,'!define PRODUCT_DIR_REGKEY "Software\Microsoft\Windows\CurrentVersion\App Paths\'&$proyecto&'.exe"');
	FileWriteLine($nsis_file,'!define PRODUCT_UNINST_KEY "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"');
	FileWriteLine($nsis_file,'!define PRODUCT_UNINST_ROOT_KEY "HKLM"');
	FileWriteLine($nsis_file,'');
	FileWriteLine($nsis_file,'; MUI 1.67 compatible ------');
	FileWriteLine($nsis_file,'!include "MUI.nsh"');
	FileWriteLine($nsis_file,'');
	FileWriteLine($nsis_file,'; MUI Settings');
	FileWriteLine($nsis_file,'!define MUI_ABORTWARNING');
	FileWriteLine($nsis_file,'!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install.ico"');
	FileWriteLine($nsis_file,'!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\modern-uninstall.ico"');
	FileWriteLine($nsis_file,'');
	FileWriteLine($nsis_file,'; Welcome page');
	FileWriteLine($nsis_file,'!insertmacro MUI_PAGE_WELCOME');
	if(FileExists($path_proyecto&"license.txt")) Then
		FileWriteLine($nsis_file,'; License page');
		FileWriteLine($nsis_file,'!insertmacro MUI_PAGE_LICENSE "license.txt"');
	EndIf
	FileWriteLine($nsis_file,'; Directory page');
	FileWriteLine($nsis_file,'!insertmacro MUI_PAGE_DIRECTORY');
	FileWriteLine($nsis_file,'; Instfiles page');
	FileWriteLine($nsis_file,'!insertmacro MUI_PAGE_INSTFILES');
	FileWriteLine($nsis_file,'; Finish page');
	FileWriteLine($nsis_file,'!define MUI_FINISHPAGE_RUN "$INSTDIR\'&$proyecto&'.exe"');
	FileWriteLine($nsis_file,'!insertmacro MUI_PAGE_FINISH');
	FileWriteLine($nsis_file,'');
	FileWriteLine($nsis_file,'; Uninstaller pages');
	FileWriteLine($nsis_file,'!insertmacro MUI_UNPAGE_INSTFILES');
	FileWriteLine($nsis_file,'');
	FileWriteLine($nsis_file,'; Language files');
	FileWriteLine($nsis_file,'!insertmacro MUI_LANGUAGE "Spanish"');
	FileWriteLine($nsis_file,'!insertmacro MUI_LANGUAGE "English"');
	FileWriteLine($nsis_file,'; MUI end ------');
	FileWriteLine($nsis_file,'');
	FileWriteLine($nsis_file,'Name "${PRODUCT_NAME} ${PRODUCT_VERSION}"');
	FileWriteLine($nsis_file,'OutFile "..\out\'&$proyecto&'-'&$ver&'.exe"');
	FileWriteLine($nsis_file,'InstallDir "$PROGRAMFILES\'&$nombre_mostrar&'"');
	FileWriteLine($nsis_file,'InstallDirRegKey HKLM "${PRODUCT_DIR_REGKEY}" ""');
	FileWriteLine($nsis_file,'ShowInstDetails show');
	FileWriteLine($nsis_file,'ShowUnInstDetails show');
	FileWriteLine($nsis_file,'');
	FileWriteLine($nsis_file,'Section "'&$nombre_mostrar&' '&$ver&'" SEC01');
	FileWriteLine($nsis_file,'SetOverwrite try');
	FileWriteLine($nsis_file,'SetOutPath "$INSTDIR"');
	FileWriteLine($nsis_file,'File /r "*.*"')
	FileWriteLine($nsis_file,'Delete "$INSTDIR\setup.nsi"');
	FileWriteLine($nsis_file,'');
	FileWriteLine($nsis_file,'CreateDirectory "$SMPROGRAMS\'&$nombre_mostrar&'"');
	FileWriteLine($nsis_file,'CreateShortCut "$SMPROGRAMS\'&$nombre_mostrar&'\'&$nombre_mostrar&'.lnk" "$INSTDIR\'&$proyecto&'.exe"');
	FileWriteLine($nsis_file,'SectionEnd');
	FileWriteLine($nsis_file,'');
	FileWriteLine($nsis_file,'Section -AdditionalIcons');
	if(IniRead($proyecto_ini,"Proyecto","Web","")<>"") Then
		FileWriteLine($nsis_file,'WriteIniStr "$SMPROGRAMS\'&$nombre_mostrar&'\Visitar ${PRODUCT_WEB_SITE}.url" "InternetShortcut" "URL" "${PRODUCT_WEB_SITE}"');
	EndIF
	FileWriteLine($nsis_file,'CreateShortCut "$SMPROGRAMS\'&$nombre_mostrar&'\Desinstalar.lnk" "$INSTDIR\uninst.exe"');
	FileWriteLine($nsis_file,'SectionEnd');
	FileWriteLine($nsis_file,'');
	FileWriteLine($nsis_file,'Section -Post');
	FileWriteLine($nsis_file,'WriteUninstaller "$INSTDIR\uninst.exe"');
	FileWriteLine($nsis_file,'WriteRegStr HKLM "${PRODUCT_DIR_REGKEY}" "" "$INSTDIR\'&$proyecto&'.exe"');
	FileWriteLine($nsis_file,'WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayName" "$(^Name)"');
	FileWriteLine($nsis_file,'WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "UninstallString" "$INSTDIR\uninst.exe"');
	FileWriteLine($nsis_file,'WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayIcon" "$INSTDIR\'&$proyecto&'.exe"');
	FileWriteLine($nsis_file,'WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "DisplayVersion" "${PRODUCT_VERSION}"');
	if(IniRead($proyecto_ini,"Proyecto","Web","")<>"") Then
		FileWriteLine($nsis_file,'WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "URLInfoAbout" "${PRODUCT_WEB_SITE}"');
	EndIf
	FileWriteLine($nsis_file,'WriteRegStr ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}" "Publisher" "${PRODUCT_PUBLISHER}"');
	FileWriteLine($nsis_file,'SectionEnd');
	FileWriteLine($nsis_file,'');
	FileWriteLine($nsis_file,'Function un.onUninstSuccess');
	FileWriteLine($nsis_file,'HideWindow');
	FileWriteLine($nsis_file,'MessageBox MB_ICONINFORMATION|MB_OK "La desinstalaci�n de $(^Name) finaliz� satisfactoriamente."');
	FileWriteLine($nsis_file,'FunctionEnd');
	FileWriteLine($nsis_file,'');
	FileWriteLine($nsis_file,'Function un.onInit');
	FileWriteLine($nsis_file,'MessageBox MB_ICONQUESTION|MB_YESNO|MB_DEFBUTTON2 "�Desea desinstalar $(^Name) junto con todos sus componentes?" IDYES +2');
	FileWriteLine($nsis_file,'Abort');
	FileWriteLine($nsis_file,'FunctionEnd');
	FileWriteLine($nsis_file,'');
	FileWriteLine($nsis_file,'Section Uninstall');
	FileWriteLine($nsis_file,'RMDir /r "$SMPROGRAMS\'&$nombre_mostrar&'"');

	Local $i=0
	Local $file
	Local $aFileList

    $aFileList = _FileListToArray($export_path, "*.*",1) ;archivos
    If @error=0 Then
		For $i = 1 to $aFileList[0]
			$file=$export_path&"\"&$aFileList[$i]
			FileWriteLine($nsis_file,'Delete "$INSTDIR\'&$aFileList[$i]&'"');
		Next
    EndIf

    $aFileList = _FileListToArray($export_path, "*.*",2) ;directorios
    If @error=0 Then
		For $i = 1 to $aFileList[0]
			$file=$export_path&"\"&$aFileList[$i]
			FileWriteLine($nsis_file,'RMDir /r "$INSTDIR\'&$aFileList[$i]&'"');
		Next
    EndIf

	FileWriteLine($nsis_file,'Delete "$INSTDIR\uninst.exe"');
	FileWriteLine($nsis_file,'RMDir "$INSTDIR"');
	FileWriteLine($nsis_file,'');
	FileWriteLine($nsis_file,'DeleteRegKey ${PRODUCT_UNINST_ROOT_KEY} "${PRODUCT_UNINST_KEY}"');
	FileWriteLine($nsis_file,'DeleteRegKey HKLM "${PRODUCT_DIR_REGKEY}"');
	FileWriteLine($nsis_file,'SetAutoClose true');
	FileWriteLine($nsis_file,'SectionEnd');
	FileClose($nsis_file)

	RunWait("utils\NSIS\makensis.exe /V1 "&$export_path&"setup.nsi")
EndFunc

Func asociar_divpmproject()
	RegWrite("HKCU\SOFTWARE\Classes\.divpmproject","","REG_SZ","divpmprojectPackage")
	RegWrite("HKCU\SOFTWARE\Classes\.pixproject","","REG_SZ","divpmprojectPackage")
	RegWrite("HKCU\SOFTWARE\Classes\divpmprojectPackage\Shell\Open\Command","","REG_SZ",'"'&@ScriptFullPath&'" "%1"')
	RegWrite("HKCU\SOFTWARE\Classes\divpmprojectPackage\DefaultIcon","","REG_SZ",'"'&@ScriptFullPath&'",2')
EndFunc

Func importar_proyecto()
	Local $file=$CmdLine[1]

	if(Not FileExists($file) or (Not StringInStr($file,".divpmproject") and Not StringInStr($file,".pixproject"))) Then
		Return
	EndIf

	estado(t("Leyendo informaci�n de proyecto..."))

	;1. Descomprimir .ini y abrirlo
	DirRemove("temp",1)
	DirCreate("temp")
	RunWait('cmd /c '&$path_7zip&' x -t7z "'&$file&'" *\proyecto.ini -otemp',"",@SW_HIDE)

	Local $aFileList

    $aFileList=_FileListToArray("temp","*.",2)
    If(@error=0) Then
		$proyecto_ini="temp\"&$aFileList[1]&"\proyecto.ini"
		if(FileExists($proyecto_ini)) Then
			$proyecto=$aFileList[1]
			if($proyecto="") Then
				MsgBox(16,t("Error"),t("Se ha producido un error grave. El proceso se cancelar�"))
				Return
			EndIf
			if(MsgBox(4,t("DIV Project Manager - Importar proyecto"),t("�Deseas importar el proyecto ")&IniRead($proyecto_ini,"Proyecto","NombreMostrar",$proyecto)&" ("&$proyecto&" "&IniRead($proyecto_ini,"Proyecto","Version","1.0")&")?")<>6) Then
				Return
			EndIf

			if(FileExists("projects\"&$proyecto)) Then
				if(MsgBox(4+48,t("Advertencia"),t("Ya existe un proyecto con este mismo nombre."&@CRLF&"Si contin�as, se BORRAR� el proyecto que tienes actualmente en la carpeta ")&$proyecto&"."&@CRLF&@CRLF&t("�Quieres continuar?"))<>6) Then
					Return
				EndIf
				DirRemove("projects\"&$proyecto,1)
				If(FileExists("projects\"&$proyecto)) Then
					MsgBox(16,t("Error"),t("No se ha podido eliminar correctamente la carpeta de proyecto anterior."&@CRLF&"Proceso de importaci�n cancelado."))
					Return
				EndIf
			EndIf

			;Importamos el proyecto! :D
			estado(t("Importando el proyecto..."))
			RunWait('cmd /c '&$path_7zip&' x -t7z "'&$file&'" -oprojects')
			MsgBox(64,t("Finalizado"),t("�Proyecto importado con �xito!"))

			IniWrite($config_file,"General","UltimoProyecto",$proyecto)
		Else
			MsgBox(16,t("Error"),t("No se ha podido leer la informaci�n del proyecto (proyecto.ini)"))
		EndIf
	Else
		MsgBox(16,t("Error"),t("No se ha descomprimido el archivo proyecto.ini"))
    EndIf
EndFunc

Func t($texto)
	;Preparando traducciones!
	$texto=StringReplace($texto,@CRLF,"@CRLF")
	if(INIRead("langs\"&$language&".txt","Translation",$texto,"")="") Then
		INIWrite("langs\"&$language&".txt","Translation",$texto,$texto)
		$texto="%"&$texto&"%"
	Else
		$texto=INIRead("langs\"&$language&".txt","Translation",$texto,"")
	EndIf
	$texto=StringReplace($texto,"@CRLF",@CRLF)
	Return $texto
EndFunc

Func actualiza()
	;Desactivado por el peligro que supone una actualizaci�n autom�tica de este tipo
	Return
	#comments-start
	Local $version_update=BinaryToString(InetRead("http://divpm.divhub.org/update.php"))
	;1. Descarga update.zip
	estado(t("Descargando actualizaci�n..."))
	InetGet("http://divpm.divhub.org/downloads/update.zip","update.zip")
	if(@error or Not FileExists("update.zip")) Then
		MsgBox(16,"Error","Se ha producido un error al intentar descargar la actualizaci�n.")
		Exit
	EndIf
	;2. Descomprime update.zip
	estado(t("Aplicando actualizaci�n..."))
	RunWait("cmd /c "&$path_7zip&" x update.zip -aoa")
	;3. Arranca DivPM.exe
	GUIDelete()
	FileDelete("update.zip")
	IniWrite($config_file,"General","Version",$version_update)
	MsgBox(64,"Actualizaci�n completada","Actualizaci�n aplicada con �xito. El programa se reiniciar�.")
	Run("divpm.exe")
	#comments-end
EndFunc

Func compara_fecha_archivos($origen, $destino)
	if(FileGetTime($origen, 0, 1)>FileGetTime($destino, 0, 1)) Then
		Return 1
	Else
		Return 0
	EndIf
EndFunc

; WIP!!!!
Func averigua_si_compilar_fpgs()
	Local $fpg_names=_FileListToArray($path_proyecto&"fpg-sources","*",2)
	Local $fallo,$fecha_fpg

	For $i=1 to UBound($fpg_names)-1
		$fallo=0
		$fecha_fpg=FileGetTime($path_proyecto&"fpg\"&$fpg_names[$i]&".fpg", 0, 1)
		For $x=1 to 999
			If(FileExists($path_proyecto&"fpg-sources\"&$fpg_names[$i]&"\"&$x&".png")) Then
				if($fecha_fpg<FileGetTime($path_proyecto&"fpg-sources\"&$fpg_names[$i]&"\"&$x&".png", 0, 1)) Then
					$fallo=1
					ExitLoop
				EndIf
			EndIf
		Next
		if($fallo) Then
			RunWait("cmd /c ..\..\..\"&$path_bgdi&" ..\..\..\utils\pxlfpg.dcb 32 "&$fpg_names[$i],$path_proyecto&"fpg-sources",@SW_HIDE)
		EndIf
	Next
EndFunc

Func convertir_graficos_a_32bpp($carpeta)
	Local $fpg_names=_FileListToArray($carpeta,"*",2)
	Local $i
	Local $archivo

	if($carpeta="") Then
		Return
	EndIf

	if(StringRight($carpeta,1)="\") Then
		$carpeta=StringLeft($carpeta,StringLen($carpeta)-1)
	EndIf

	;subcarpetas
	For $i=1 to UBound($fpg_names)-1
		convertir_graficos_a_32bpp($carpeta&"\"&$fpg_names[$i])
	Next

	estado("Convirtiendo PNGs ("&StringMid($carpeta,StringInStr($carpeta,"\",0,-1)+1)&")")

	;convertimos gr�ficos
	For $x=1 to 999
		If(FileExists($carpeta&"\"&$x&".png")) Then
			$archivo=$carpeta&"\"&$x&".png"
			RunWait('utils\ImageMagick\convert "'&$archivo&'" png32:"'&$archivo&'"',@ScriptDir,@SW_HIDE)
		EndIf
	Next
EndFunc